
import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';



const ServerUrl = "http://www.tapper.co.il/salecar/laravel/public/api/";
@Injectable()

export class CompanyService
{
    public headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    public options = new RequestOptions({headers:this.headers});

    public Companies:any[]=[];
    public CompanyArray:any[]=[];

    constructor(private http:Http) { };

    GetAllCompanies(url:string)
    {
        let body = new FormData();
        body.append('uid', window.localStorage.identify);
        return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{this.Companies = data }).toPromise();
    }

    AddCompany(url,Company)
    {
        this.CompanyArray = Company;
        let body = 'company=' + JSON.stringify(this.CompanyArray);
        return this.http.post(ServerUrl + '' + url, body, this.options).map(res => res).do((data)=>{}).toPromise();
    }

    EditCompany(url,Company)
    {
        this.CompanyArray = Company;
        let body = 'company=' + JSON.stringify(this.CompanyArray);
        return this.http.post(ServerUrl + '' + url, body, this.options).map(res => res).do((data)=>{}).toPromise();
    }

    DeleteCompany(url,Id)
    {
        let body = 'id=' + Id;
        return this.http.post(ServerUrl + '' + url, body, this.options).map(res => res.json()).do((data)=>{}).toPromise();
    }
};


